from flask import Flask, render_template
app = Flask(__name__, template_folder="template")

@app.route('/<id>')
def index(id):
    return render_template("index.html")

if __name__ == '__main__':
     app.run(host='0.0.0.0', port=3238) 
