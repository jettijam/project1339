#-*- coding:utf-8 -*-
from sanic import Sanic
import json
from sanic import response
import pymongo
from bson.objectid import ObjectId

app = Sanic()
conn = pymongo.MongoClient("127.0.0.1", 27017)
db = conn.test
collection = db.real



@app.route("/home")
async def home(request):
    return json({"hello":"world"})

@app.route("/question/<node_id>")
async def question(request, node_id):
    result = collection.find_one({'Id':int(node_id)})
    result.pop('_id', None)
    json_val = json.dumps(result)
    return response.json(json_val, headers={ 'Content-Type': 'application/json; charset=UTF-8',
                                      'Access-Control-Allow-Origin':'*'})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=6435)
